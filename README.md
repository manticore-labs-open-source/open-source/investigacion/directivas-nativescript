# Directivas de nativescript
## Indice
1. [Introduccion](#id1)
2.  [Clasificación](#id2)

### keywords **nativescript, angular, directivas, movil, development**
<a name="id1"></a>
## Introduccion 
La directivas utilzadas en nativescript son las mismas que las utilizadas en angular a continuacion se detalla cada una de las mismas. Las cuales nos permiten manipulacion y reutilizacion de codigo HTML.
Las directivas son la técnica que nos va a permitir crear nuestros propios componentes visuales
encapsulando las posibles complejidades en la implementación, normalizando y parametrizándolos según nuestras necesidades.

<a name="id2"></a>
## Clasificación
### NgFor 
 nos permite generar muchos elementos HTML repetidos a partir del recorrido de un arreglo de datos.
 ejemplo:
 componetent.html
 ~~~~
<ScrollView sdkExampleTitle sdkToggleNavButton>
    <StackLayout class="list-group">
        <StackLayout *ngFor="let fruit of fruitList" class="list-group-item">
            <Label [text]="fruit"></Label>
        </StackLayout>
    </StackLayout>
</ScrollView>
~~~~
component.ts
~~~~
import { Component } from "@angular/core";
import { fruits } from "./fruits";
@Component({
    moduleId: module.id,
    templateUrl: "./ngfor-directive.component.html",
})
export class NgForComponent {
    public fruitList: Array<string> = [
    "Apple", "Apricot", "Avocado", "Banana", "Bilberry", "Blackberry", "Blackcurrant", "Blueberry", "Boysenberry", "Currant", "Cherry", "Cherimoya", "Cloudberry"];
    constructor() {}
}
~~~~
vista:
![formulas](imagenes/ngfor.jpeg)
### NgIf
condicionar si dicha marca debe agregarse a la página HTML.
ejemplo:
 componetent.html
 ~~~~
<StackLayout sdkExampleTitle sdkToggleNavButton>
    <StackLayout>
        <!-- >> using-ngif-html -->
        <Button text="Mostra/Ocultar bloque" (tap)="onTap()" class="btn btn-primary btn-active"></Button>
        <GridLayout *ngIf="isVisible" class="bg-primary" borderRadius="2" height="300"></GridLayout>
        <!-- << using-ngif-html -->
    </StackLayout>
</StackLayout>>
~~~~
component.ts
~~~~
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    templateUrl: "./ngif-directive.component.html",
})
export class NgIfComponent {
    public isVisible: boolean = true;

    onTap() {
        if (this.isVisible) {
            this.isVisible = false;
        } else  {
            this.isVisible = true;
        }
    }
}
~~~~

vista:
![formulas](imagenes/ngif.jpeg)
### NgSwitch
La directiva ng-switch es similar a ng-if y como nos podemos imaginar es como el switch de la programación. Es decir que permite que entre varios conjuntos de tags solo esté uno de ellos, borrando los que no cumplen la condición.
ejemplo:
 componetent.html
 ~~~~
<GridLayout rows="100, *" columns="*" sdkExampleTitle sdkToggleNavButton>
    <!-- >> using-ngswitch-html -->
    <FlexboxLayout row="0" flexDirection="row">
        <Button flexShrink="0" class="btn btn-primary btn-active" margin="15"
            width="80" text="Azul" (tap)="onBlue()"></Button>
        <Button flexShrink="0" class="btn btn-primary btn-active" margin="15"
            width="80" text="Morado" (tap)="onPurple()"></Button>
        <Button flexShrink="0" class="btn btn-primary btn-active" margin="15"
            width="80" text="Amarrillo" (tap)="onYellow()"></Button>
    </FlexboxLayout>

    <GridLayout row="1" [ngSwitch]="color" class="p-15 m-t-15" height="280"
        borderRadius="2">
        <GridLayout *ngSwitchCase="'purple'" backgroundColor="#8C489F"></GridLayout>
        <GridLayout *ngSwitchCase="'blue'" backgroundColor="#0077AF"></GridLayout>
        <GridLayout *ngSwitchCase="'yellow'" backgroundColor="#FFFF66"></GridLayout>
        <GridLayout *ngSwitchDefault backgroundColor="gray"></GridLayout>
    </GridLayout>
    <!-- << using-ngswitch-html -->
</GridLayout>
~~~~
component.ts
~~~~
import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    templateUrl: "./ngswitch-directive.component.html",
})
export class NgSwitchComponent {
    public color: string;

    onBlue() {
        this.color = "blue";
    }

    onPurple() {
        this.color = "purple";
    }

    onYellow() {
        this.color = "yellow";
    }
}
~~~~
vista:
![formulas](imagenes/ngswitch-1.jpeg)

![formulas](imagenes/ngswitch-2.jpeg)

![formulas](imagenes/ngswitch-3.jpeg)

## Redes Sociales
<a href="https://twitter.com/crisjc8" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @crisjc8 </a><br>
<a href="https://linkedin.com/in/cristhian-jumbo-748934180/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Cristhian Jumbo</a><br>
<a href="https://www.instagram.com/crisjc6/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> crisjc6</a><br>
